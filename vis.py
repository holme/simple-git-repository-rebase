#!/usr/bin/env python3

import numpy as np


x = np.linspace(-5, 5, 1001, endpoint=True)

y = np.exp(-x**2)

_maxpos = np.argmax(y)
_max = y[_maxpos]

print("Maximum is {:.2} at {:.2}.".format(_max, x[_maxpos]))
